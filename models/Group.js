//const autoIncrement = require('mongoose-auto-increment');
module.exports = mongoose => {
    const newSchema = new mongoose.Schema({
        name: {
            type: String
        },
        description: {
            type: String
        },
        Isactive: {
            type: Boolean
        }
    }, {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    });
    //newSchema.plugin(autoIncrement.plugin, 'groups');
    const Group = mongoose.model('Group', newSchema);
    return Group;
};