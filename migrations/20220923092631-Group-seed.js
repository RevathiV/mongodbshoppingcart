module.exports = {
  async up(db) {
      return await db.collection('Group').insertMany([{
        name: 'mens wear',
        description:"Men's clothes are articles of clothing designed for and worn by men",
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),
      },{
        name: 'womens wear',
        description:"Women's clothes are articles of clothing designed for and worn by women",
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {})
  },
  async down(db) {
      return await db.collection('Group').deleteMany({})
  },
}