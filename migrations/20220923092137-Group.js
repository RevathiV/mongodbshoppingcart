const groups = require("../models/Group")
module.exports = {
    async up(db) {
        return await db.createCollection('Group')
    },
    async down(db) {
        return await db.collection('Group').drop()
    },
}