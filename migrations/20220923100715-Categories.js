const groups = require("../models/Categories")
module.exports = {
    async up(db) {
        return await db.createCollection('category')
    },
    async down(db) {
        return await db.collection('category').drop()
    },
}
