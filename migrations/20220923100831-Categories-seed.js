module.exports = {
  async up(db) {
      return await db.collection('Categories').insertMany([{
        name: 'men formal wear',
        description:'formal clothing includes trousers, some form of neckwear suit accessory and dress shoes.',
        GroupId:1,
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),
  
      },{
        name: 'women formal wear',
        description:'it includes kurtas, jeans, tops',
        GroupId:2,
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),

      }], {})
  },
  async down(db) {
      return await db.collection('Categories').deleteMany({})
  },
}
