//const groups = require("../models/Products")
module.exports = {
    async up(db) {
        return await db.createCollection('products')
    },
    async down(db) {
        return await db.collection('products').drop()
    },
}